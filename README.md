# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

* **ids ,variables and Functions**
    * myCanvas:為主畫版的id，此畫版預設白底，所有畫完一個步驟的圖都是直接畫在主畫版上
    * myCanvas2:為副畫版的id，此畫版預設透明，且蓋在主畫版上方，大部分未畫完一個步驟的圖都畫在副畫版上，主要當預覽畫面
    * init():把一些東西的初始值訂好&將背景填滿白色&存第一次檔
    * changebold(bool),changeface(face),changefont():更改粗體與否&字體
    * changefill(bool):更改是否填滿圖形&文字
    * changesize(size):更改字形&圖形線段寬的大小
    * reset():為第一個工具箱中的功能，即將之前畫過的東西都清掉，但仍會留下紀錄(可undo)
    * undoarray:用來存undo時要顯示的畫面
    * save():存檔功能，其實就只是把現在的畫面存下來並丟進undoarray中
    * doUndo():在undoarray中顯示上一個畫面
    * doRedo():在undoarray中顯示下一個畫面
    * gun():為第6個工具箱中的功能，當使用者按下噴槍按鍵時，透過這個函數呼叫 drawgun()
    * drawgun():每1ms隨機噴一堆點在有效範圍內，透過gun()被呼叫，且被restate()停止
    * state:共有11種state: 0:無使用中 1:筆刷 2:擦布 3:噴槍 4:畫圓 5:畫長方形 6:畫三角形 7:印字 8:印圖片 9:複製 10:貼上 11:剪下
    * restate():更新狀態時被呼叫，一開始的功能只有更改state的值而已，但後來功能大量擴增。現在會將副畫版、已載入的圖片清空，並把一些變數弄回初始值，也停止drawgun()的運作。另外也掌管了功能正在使用中時，按鈕變成半透明以及游標換成按鈕圖案的特效
    * draw: 1:正在畫(滑鼠已按下) 0:沒在畫(滑鼠已放開)
    * md():mousedown時呼叫的函數，依不同的state做不同的起始動作，也會將draw改成1，並在最後呼叫第一次mv()
    * mv():mousemove時呼叫的函數，除了筆刷或擦布或噴槍的state以外都是在做預覽圖
    * mp():mouseupp時呼叫的函數，依不同的state做不同的結束動作，也會將draw改成0，和將副畫版清空
    * readimg(files),setimg(img):掌管上傳圖片的部分，將使用者上傳的圖片存在canvas後方，並將滑鼠游標改成該圖

* **補充**
    * 筆刷、擦布、噴槍、圓形和長方形用拖曳的來作圖
    * 三角形因為有三個點要決定，所以是用點三點來作圖
    * 印字是會印出下方的type here中的字，方便大量印同樣的字
    * 印圖片時，若圖片夠小，游標也會變成該圖的樣子
